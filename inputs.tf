variable "location" {
  type = string
}

variable "name" {
  type = string
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "resource_group_name" {
  type = string
}

variable "expected_kv_passwords" {
  type = set(string)
}

variable "length" {
  type    = number
  default = 20
}

variable "roles" {
  type = list(object({
    role         = string
    principal_id = string
  }))
  default = []
}
