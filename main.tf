data "azurerm_client_config" "current" {}


resource "azurerm_key_vault" "kv" {
  name                            = var.name
  location                        = var.location
  resource_group_name             = var.resource_group_name
  sku_name                        = "standard"
  tenant_id                       = data.azurerm_client_config.current.tenant_id
  enabled_for_template_deployment = false
  enable_rbac_authorization       = true
  soft_delete_retention_days      = 30
  purge_protection_enabled        = false
  tags                            = var.tags
}

data "azurerm_key_vault_secrets" "secrets" {
  key_vault_id = azurerm_key_vault.kv.id
}

resource "random_password" "password" {
  for_each    = var.expected_kv_passwords
  length      = var.length
  min_upper   = 2
  min_lower   = 2
  min_numeric = 2
  min_special = 2
  keepers     = {
    name = each.value
  }
}

resource "azurerm_key_vault_secret" "expected_secrets" {
  for_each     = var.expected_kv_passwords
  key_vault_id = azurerm_key_vault.kv.id
  name         = each.value
  value        = try(data.azurerm_key_vault_secrets.secrets.names[each.value], random_password.password[each.value].result)

  lifecycle {
    ignore_changes = [
      tags,
      value,
    ]
  }
}


resource "azurerm_role_assignment" "roles" {
  for_each = {
    for val in coalesce(var.roles, []) : "${val.role}>${val.principal_id}" => val
  }

  scope                = azurerm_key_vault.kv.id
  role_definition_name = each.value.role
  principal_id         = each.value.principal_id
}
