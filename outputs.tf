output "id" {
  value = azurerm_key_vault.kv.id
}
output "name" {
  value = azurerm_key_vault.kv.name
}
output "expected_secrets" {
  value = azurerm_key_vault_secret.expected_secrets
}
