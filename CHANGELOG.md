# Unreleased
## Added
## Changed
## Fixed

# [0.3.1] - 2025-01-08
## Added
- Priviledge roles

# [0.3.0] - 2023-08-11
## Changed
- BC BREAK: default value `enabled_for_template_deployment` chnaged from `true` -> `false` 

# [0.2.0] - 2023-01-02
## Added
- add tags
## Changed
## Fixed